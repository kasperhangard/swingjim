﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBPlayerController : MonoBehaviour
{
    public float Speed = 5f;
    public float JumpHeight = 2f;
    public float Acceleration = 20f;
    public float AirAcceleration = 20f;
    public float GroundDistance = 0.2f;
    public float DashDistance = 5f;
    public LayerMask Ground;

    private Rigidbody _body;
    private Vector3 _inputs = Vector3.zero;
    private bool _isGrounded = true;
    private Transform _groundChecker;

    Vector3 lastPos;

    public static Rigidbody rb;

    void Start()
    {
        _body = GetComponent<Rigidbody>();
        _groundChecker = transform.GetChild(0);
        lastPos = transform.position;
        rb = _body;
    }

    void Update()
    {
        _isGrounded = Physics.CheckSphere(_groundChecker.position, GroundDistance, Ground, QueryTriggerInteraction.Ignore);


        _inputs = Vector3.zero;
        _inputs.x = Input.GetAxis("Horizontal");
        _inputs.z = Input.GetAxis("Vertical");
        _inputs = transform.TransformDirection(_inputs);

        Vector3 forceVector = Vector3.zero;

        if(Input.GetAxis("Horizontal") > 0 && _body.velocity.x < Speed)
        {
            forceVector.x += Input.GetAxis("Horizontal");
        } else if(Input.GetAxis("Horizontal") < 0 && _body.velocity.x > -Speed)
        {
            forceVector.x += Input.GetAxis("Horizontal");
        }

        if (Input.GetAxis("Vertical") > 0 && _body.velocity.z < Speed)
        {
            forceVector.z += Input.GetAxis("Vertical");
        }
        else if (Input.GetAxis("Vertical") < 0 && _body.velocity.z > -Speed)
        {
            forceVector.z += Input.GetAxis("Vertical");
        }

        forceVector = transform.TransformDirection(forceVector);

        //if (_isGrounded)
        //{
        //    _body.AddForce(forceVector * Acceleration * Time.deltaTime);
        //} else
        //{
        //    _body.AddForce(forceVector * AirAcceleration * Time.deltaTime);
        //}


        Accelerate(_inputs, Speed, Acceleration);

        if (Input.GetKeyDown(KeyCode.Space) && _isGrounded)
        {
            _body.AddForce(Vector3.up * Mathf.Sqrt(JumpHeight * -2f * Physics.gravity.y), ForceMode.VelocityChange);
        }

    }





    private void Accelerate(Vector3 wishdir, float wishspeed, float accel)
    {
        float addspeed;
        float accelspeed;
        float currentspeed;

        currentspeed = Vector3.Dot(_body.velocity, wishdir);
        addspeed = wishspeed - currentspeed;
        if (addspeed <= 0)
            return;
        
        accelspeed = accel * Time.deltaTime * wishspeed;
        if (accelspeed > addspeed)
            accelspeed = addspeed;

        Vector3 movementVector = new Vector3(accelspeed * wishdir.x, 0, accelspeed * wishdir.z);

        Debug.Log(movementVector);

        _body.AddForce(movementVector);

    }



}
